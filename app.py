from flask import Flask, request, jsonify
from datetime import datetime, timedelta
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    create_access_token,
    get_jwt_identity,
)
from flask_wtf.csrf import CSRFProtect
import pymssql
import bcrypt
import re
import dotenv
dotenv.load_dotenv()
import os

app = Flask(__name__)
CSRFProtect(app)
app.config["SECRET_KEY"] = "GKZ4VCiB7vZD5ixC"
app.config[
    "JWT_SECRET_KEY"
] = "xySuQW2Ltioq2Yij"  # Clave secreta para JWT (cámbiala por una segura)

# Obtener las credenciales de la base de datos desde variables de entorno
server = os.getenv("server")
username = os.getenv("username")
password = os.getenv("password")
database = os.getenv("database")



# Función para establecer una conexión a la base de datos
def conectar():
    try:
        conn = pymssql.connect(server=server, user=username, password=password, database=database)
        return conn
    except Exception as e:
        raise Exception(f"Error de conexion a la base de datos: {str(e)}")

# Configuración de Flask-JWT-Extended
jwt = JWTManager(app)

# Lista de usuarios autorizados
usuarios_autorizados = {
    "daf3r": "bruh",
    "alfredo": "pepe123",
}


# Ruta para autenticar un usuario y obtener un token JWT
@app.route("/login", methods=["POST"])
def login():
    try:
        data = request.json
        if "username" not in data or "password" not in data:
            return (
                jsonify({"error": "Se requieren nombre de usuario y contraseña"}),
                400,
            )

        # Verifica las credenciales del usuario y aplica restricciones
        username = data["username"]
        password = data["password"]

        if (
            username in usuarios_autorizados
            and usuarios_autorizados[username] == password
        ):
            # Calcula la hora de medianoche (00:00:00) en formato UTC
            now = datetime.utcnow()
            midnight = datetime(now.year, now.month, now.day) + timedelta(days=1)

            # Calcula la diferencia en tiempo hasta la medianoche
            expires = midnight - now

            # Crea un token JWT para el usuario autorizado con una expiración
            access_token = create_access_token(identity=username, expires_delta=expires)
            return jsonify(access_token=access_token), 200
        else:
            return jsonify({"error": "Credenciales inválidas"}), 401
    except Exception as e:
        return jsonify({"error": str(e)})


# LABORATORIO
# Ruta para obtener todos los laboratorios (requiere autenticación)
@app.route("/laboratorio", methods=["GET"])
#@jwt_required()
def obtener_laboratorios():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute("SELECT id_laboratorio, nombre FROM Laboratorio")
        laboratorios = []
        for row in cursor.fetchall():
            laboratorios.append({"id_laboratorio": row[0], "nombre": row[1]})
        conn.close()
        return jsonify(laboratorios)
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para crear un nuevo laboratorio
@app.route("/laboratorio", methods=["POST"])
@jwt_required()
def crear_laboratorio():
    try:
        data = request.json
        if "nombre" not in data:
            return jsonify({"error": 'El campo "nombre" es obligatorio'}), 400

        conn = conectar()
        cursor = conn.cursor()
        query = "INSERT INTO Laboratorio (nombre) VALUES (%s)"
        cursor.execute(query, (data["nombre"],))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Laboratorio creado con exito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para actualizar un laboratorio existente
@app.route("/laboratorio/<int:id_laboratorio>", methods=["PUT"])
@jwt_required()
def actualizar_laboratorio(id_laboratorio):
    try:
        data = request.json
        if "nombre" not in data:
            return jsonify({"error": 'El campo "nombre" es obligatorio'}), 400

        conn = conectar()
        cursor = conn.cursor()
        query = "UPDATE Laboratorio SET nombre = %s WHERE id_laboratorio = %s"
        cursor.execute(query, (data["nombre"], id_laboratorio))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Laboratorio actualizado con exito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para eliminar un laboratorio existente
@app.route("/laboratorio/<int:id_laboratorio>", methods=["DELETE"])
@jwt_required()
def eliminar_laboratorio(id_laboratorio):
    try:
        conn = conectar()
        cursor = conn.cursor()
        query = "DELETE FROM Laboratorio WHERE id_laboratorio = %s"
        cursor.execute(query, (id_laboratorio,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Laboratorio eliminado con exito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# VEHICULO
# Obtener el vehiculo
@app.route("/vehiculo", methods=["GET"])
@jwt_required()
def obtener_vehiculos():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT id_vehiculo, matricula, tipo_vehiculo, descripcion FROM Vehiculo"
        )
        vehiculos = []
        for row in cursor.fetchall():
            vehiculos.append(
                {
                    "id_vehiculo": row[0],
                    "matricula": row[1],
                    "tipo_vehiculo": row[2],
                    "descripcion": row[3],
                }
            )
        conn.close()
        return jsonify(vehiculos)
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para ingresar un nuevo vehiculo
@app.route("/vehiculo", methods=["POST"])
@jwt_required()
def crear_vehiculo():
    try:
        data = request.json
        # Verifica que al menos se proporcione la matrícula y el tipo de vehículo
        if "matricula" not in data or "tipo_vehiculo" not in data:
            return (
                jsonify(
                    {
                        "error": 'Los campos "matricula" y "tipo_vehiculo" son obligatorios'
                    }
                ),
                400,
            )

        # Verifica si la matrícula ya existe en la base de datos
        conn = conectar()
        cursor = conn.cursor()
        matricula = data["matricula"]
        cursor.execute(
            "SELECT COUNT(*) FROM Vehiculo WHERE matricula = %s", (matricula,)
        )
        count = cursor.fetchone()[0]
        conn.close()

        if count > 0:
            return jsonify({"error": "La matrícula ya existe en la base de datos"}), 400

        # Si la matrícula es única, procede con la inserción
        conn = conectar()
        cursor = conn.cursor()
        query = "INSERT INTO Vehiculo (matricula, tipo_vehiculo, descripcion) VALUES (%s, %s, %s)"
        cursor.execute(
            query, (matricula, data["tipo_vehiculo"], data.get("descripcion", ""))
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Vehículo creado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para actualizar un vehículo por su ID
@app.route("/vehiculo/<int:id_vehiculo>", methods=["PUT"])
@jwt_required()
def actualizar_vehiculo(id_vehiculo):
    try:
        data = request.json
        conn = conectar()
        cursor = conn.cursor()

        # Verifica si el vehículo existe por su ID
        cursor.execute(
            "SELECT COUNT(*) FROM Vehiculo WHERE id_vehiculo = %s", (id_vehiculo,)
        )
        count = cursor.fetchone()[0]

        if count == 0:
            conn.close()
            return jsonify({"error": "El vehículo no existe"}), 404

        # Verifica si la matrícula ya existe en otro vehículo
        if "matricula" in data:
            cursor.execute(
                "SELECT COUNT(*) FROM Vehiculo WHERE matricula = %s AND id_vehiculo != %s",
                (data["matricula"], id_vehiculo),
            )
            count = cursor.fetchone()[0]

            if count > 0:
                conn.close()
                return (
                    jsonify({"error": "La matrícula ya existe en otro vehículo"}),
                    400,
                )

        # Construye la consulta de actualización
        update_query = "UPDATE Vehiculo SET "
        update_params = []

        if "matricula" in data:
            update_query += "matricula = %s, "
            update_params.append(data["matricula"])

        if "tipo_vehiculo" in data:
            update_query += "tipo_vehiculo = %s, "
            update_params.append(data["tipo_vehiculo"])

        if "descripcion" in data:
            update_query += "descripcion = %s, "
            update_params.append(data["descripcion"])

        # Elimina la última coma y espacio en blanco del query
        update_query = update_query[:-2]

        # Agrega la condición WHERE para actualizar por ID
        update_query += " WHERE id_vehiculo = %s"
        update_params.append(id_vehiculo)

        cursor.execute(update_query, tuple(update_params))
        conn.commit()
        conn.close()

        return jsonify({"mensaje": "Vehículo actualizado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para eliminar un vehículo existente
@app.route("/vehiculo/<int:id_vehiculo>", methods=["DELETE"])
@jwt_required()
def eliminar_vehiculo(id_vehiculo):
    try:
        conn = conectar()
        cursor = conn.cursor()
        query = "DELETE FROM Vehiculo WHERE id_vehiculo = %s"
        cursor.execute(query, (id_vehiculo,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Vehículo eliminado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# DELIVERY
# Ruta para obtener los datos
@app.route("/delivery", methods=["GET"])
@jwt_required()
def obtener_delivery():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT id_delivery, vehiculo_id, dui, nombre, direccion, telefono FROM Delivery"
        )
        deliverys = []
        for row in cursor.fetchall():
            deliverys.append(
                {
                    "id_delivery": row[0],
                    "vehiculo_id": row[1],
                    "dui": row[2],
                    "nombre": row[3],
                    "direccion": row[4],
                    "telefono": row[5],
                }
            )
        conn.close()
        return jsonify(deliverys)
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para registrar un nuevo delivery
@app.route("/delivery", methods=["POST"])
@jwt_required()
def crear_delivery():
    try:
        data = request.json
        if (
            "dui" not in data
            or "nombre" not in data
            or "direccion" not in data
            or "telefono" not in data
        ):
            return (
                jsonify(
                    {
                        "error": 'Los campos "dui", "nombre", "direccion" y "telefono" son obligatorios'
                    }
                ),
                400,
            )

        conn = conectar()
        cursor = conn.cursor()
        query = "INSERT INTO Delivery (vehiculo_id, dui, nombre, direccion, telefono) VALUES (%s, %s, %s, %s, %s)"
        cursor.execute(
            query,
            (
                data.get("vehiculo_id"),
                data["dui"],
                data["nombre"],
                data["direccion"],
                data["telefono"],
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Delivery creado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para actualizar un delivery existente
@app.route("/delivery/<int:id_delivery>", methods=["PUT"])
@jwt_required()
def actualizar_delivery(id_delivery):
    try:
        data = request.json
        conn = conectar()
        cursor = conn.cursor()

        # Recupera el delivery existente de la base de datos
        query = "SELECT vehiculo_id, dui, nombre, direccion, telefono FROM Delivery WHERE id_delivery = %s"
        cursor.execute(query, (id_delivery,))
        delivery_existente = cursor.fetchone()

        # Verifica que el delivery exista
        if delivery_existente is None:
            return jsonify({"error": "Delivery no encontrado"}), 404

        # Actualiza solo los campos proporcionados en la solicitud PUT
        if "dui" in data:
            delivery_existente["dui"] = data["dui"]
        if "nombre" in data:
            delivery_existente["nombre"] = data["nombre"]
        if "direccion" in data:
            delivery_existente["direccion"] = data["direccion"]
        if "telefono" in data:
            delivery_existente["telefono"] = data["telefono"]

        # Actualiza los campos del delivery en la base de datos
        query = "UPDATE Delivery SET dui = %s, nombre = %s, direccion = %s, telefono = %s WHERE id_delivery = %s"
        cursor.execute(
            query,
            (
                delivery_existente["dui"],
                delivery_existente["nombre"],
                delivery_existente["direccion"],
                delivery_existente["telefono"],
                id_delivery,
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Delivery actualizado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para eliminar un dato
@app.route("/delivery/<int:id_delivery>", methods=["DELETE"])
@jwt_required()
def eliminar_delivery(id_delivery):
    try:
        conn = conectar()
        cursor = conn.cursor()
        query = "DELETE FROM Delivery WHERE id_delivery = %s"
        cursor.execute(query, (id_delivery,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Delivery eliminado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# CLIENTES
@app.route("/cliente", methods=["GET"])
# @jwt_required()
def obtener_cliente():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT id_cliente, nombre, direccion, telefono, correo, password_hash FROM Cliente"
        )
        proveedores = []
        for row in cursor.fetchall():
            proveedores.append(
                {
                    "id_cliente": row[0],
                    "nombre": row[1],
                    "direccion": row[2],
                    "telefono": row[3],
                    "correo": row[4],
                    "password_hash": row[5],
                }
            )
        conn.close()
        return jsonify(proveedores)
    except Exception as e:
        return jsonify({"error": str(e)})


@app.route("/cliente", methods=["POST"])
# @jwt_required()
def crear_cliente():
    try:
        data = request.json
        if (
            "nombre" not in data
            or "direccion" not in data
            or "telefono" not in data
            or "correo" not in data
            or "password" not in data
        ):
            return (
                jsonify(
                    {
                        "error": 'Los campos "nombre", "direccion", "telefono", "correo" y "password" son obligatorios'
                    }
                ),
                400,
            )

        # Hashea la contraseña antes de almacenarla en la base de datos
        password_plano = data["password"]
        password_hasheada = bcrypt.hashpw(
            password_plano.encode("utf-8"), bcrypt.gensalt()
        )

        conn = conectar()
        cursor = conn.cursor()
        query = "INSERT INTO Cliente (nombre, direccion, telefono, correo, password_hash) VALUES (%s, %s, %s, %s, %s)"
        cursor.execute(
            query,
            (
                data["nombre"],
                data["direccion"],
                data["telefono"],
                data["correo"],
                password_hasheada,
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Cliente creado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para actualizar un cliente existente
@app.route("/cliente/<int:id_cliente>", methods=["PUT"])
@jwt_required()
def actualizar_cliente(id_cliente):
    try:
        data = request.json
        conn = conectar()
        cursor = conn.cursor()

        # Recupera el cliente existente de la base de datos
        query = "SELECT nombre, direccion, telefono, correo, password_hash FROM Cliente WHERE id_cliente = %s"
        cursor.execute(query, (id_cliente,))
        cliente_existente = cursor.fetchone()

        # Verifica que el cliente exista
        if cliente_existente is None:
            return jsonify({"error": "Cliente no encontrado"}), 404

        # Actualiza solo los campos proporcionados en la solicitud PUT
        if "nombre" in data:
            cliente_existente["nombre"] = data["nombre"]
        if "direccion" in data:
            cliente_existente["direccion"] = data["direccion"]
        if "telefono" in data:
            cliente_existente["telefono"] = data["telefono"]
        if "correo" in data:
            cliente_existente["correo"] = data["correo"]

        # Hashea la nueva contraseña antes de actualizarla en la base de datos (si se proporciona)
        password_plano = data.get("password")
        if password_plano:
            password_hasheada = bcrypt.hashpw(
                password_plano.encode("utf-8"), bcrypt.gensalt()
            )
            cliente_existente["password_hash"] = password_hasheada

        # Actualiza los campos del cliente en la base de datos
        query = "UPDATE Cliente SET nombre = %s, direccion = %s, telefono = %s, correo = %s, password_hash = %s WHERE id_cliente = %s"
        cursor.execute(
            query,
            (
                cliente_existente["nombre"],
                cliente_existente["direccion"],
                cliente_existente["telefono"],
                cliente_existente["correo"],
                cliente_existente["password_hash"],
                id_cliente,
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Cliente actualizado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para eliminar un cliente existente
@app.route("/cliente/<int:id_cliente>", methods=["DELETE"])
@jwt_required()
def eliminar_cliente(id_cliente):
    try:
        conn = conectar()
        cursor = conn.cursor()
        query = "DELETE FROM Cliente WHERE id_cliente = %s"
        cursor.execute(query, (id_cliente,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Cliente eliminado con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# PROVEEDOR
# Ruta para obtener proveedores
@app.route("/proveedor", methods=["GET"])
@jwt_required()
def obtener_proveedor():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT id_farmacia, NFarmacia, DirFarmacia, correo, telefono, nombre_contacto FROM Proveedor"
        )
        proveedores = []
        for row in cursor.fetchall():
            proveedores.append(
                {
                    "id_farmacia": row[0],
                    "NFarmacia": row[1],
                    "DirFarmacia": row[2],
                    "correo": row[3],
                    "telefono": row[4],
                    "nombre_contacto": row[5],
                }
            )
        conn.close()
        return jsonify(proveedores)
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para crear un nuevo proveedor
@app.route("/proveedor", methods=["POST"])
@jwt_required()
def crear_proveedor():
    try:
        data = request.json
        campos_obligatorios = [
            "NFarmacia",
            "DirFarmacia",
            "correo",
            "telefono",
            "nombre_contacto",
        ]

        # Verifica si todos los campos obligatorios están presentes
        if not all(campo in data for campo in campos_obligatorios):
            return (
                jsonify(
                    {
                        "error": 'Los campos "NFarmacia", "DirFarmacia", "correo", "telefono" y "nombre_contacto" son obligatorios'
                    }
                ),
                400,
            )

        # Validación de formato de correo electrónico
        correo = data["correo"]
        if not re.match(r"[^@]+@[^@]+\.[^@]+", correo):
            return (
                jsonify({"error": "El formato del correo electrónico no es válido"}),
                400,
            )

        conn = conectar()
        cursor = conn.cursor()

        # Intenta realizar la inserción en una transacción
        try:
            query = "INSERT INTO Proveedor (NFarmacia, DirFarmacia, correo, telefono, nombre_contacto) VALUES (%s, %s, %s, %s, %s)"
            cursor.execute(
                query,
                (
                    data["NFarmacia"],
                    data["DirFarmacia"],
                    data["correo"],
                    data["telefono"],
                    data["nombre_contacto"],
                ),
            )
            conn.commit()
            conn.close()
            return jsonify({"mensaje": "Proveedor creado con éxito"})
        except pymssql.IntegrityError as e:
            conn.rollback()
            if "correo" in str(e) and "unique" in str(e):
                return (
                    jsonify(
                        {"error": "El correo electrónico ya existe en la base de datos"}
                    ),
                    400,
                )
            return jsonify({"error": "Error al crear el proveedor"}), 400
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para actualizar un proveedor existente
@app.route("/proveedor/<int:id_proveedor>", methods=["PUT"])
@jwt_required()
def actualizar_proveedor(id_proveedor):
    try:
        data = request.json
        conn = conectar()
        cursor = conn.cursor()

        # Verifica si el proveedor existe
        cursor.execute(
            "SELECT 1 FROM Proveedor WHERE id_farmacia = %s", (id_proveedor,)
        )
        existe_proveedor = cursor.fetchone()
        if not existe_proveedor:
            conn.close()
            return jsonify({"error": "Proveedor no encontrado"}), 404

        # Actualiza solo los campos proporcionados en la solicitud PUT
        campos_actualizados = {}
        if "NFarmacia" in data:
            campos_actualizados["NFarmacia"] = data["NFarmacia"]
        if "DirFarmacia" in data:
            campos_actualizados["DirFarmacia"] = data["DirFarmacia"]
        if "correo" in data:
            campos_actualizados["correo"] = data["correo"]
        if "telefono" in data:
            campos_actualizados["telefono"] = data["telefono"]
        if "nombre_contacto" in data:
            campos_actualizados["nombre_contacto"] = data["nombre_contacto"]

        if campos_actualizados:
            # Actualiza los campos del proveedor en la base de datos
            query = "UPDATE Proveedor SET "
            query += ", ".join(
                [f"{campo} = %s" for campo in campos_actualizados.keys()]
            )
            query += " WHERE id_farmacia = %s"

            valores_actualizados = tuple(campos_actualizados.values()) + (id_proveedor,)
            cursor.execute(query, valores_actualizados)
            conn.commit()
            conn.close()
            return jsonify({"mensaje": "Proveedor actualizado con éxito"})
        else:
            conn.close()
            return jsonify({"mensaje": "No se proporcionaron datos para actualizar"})

    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para eliminar un proveedor
@app.route("/proveedor/<int:id_proveedor>", methods=["DELETE"])
@jwt_required()
def eliminar_proveedor(id_proveedor):
    try:
        conn = conectar()
        cursor = conn.cursor()

        # Verifica si el proveedor existe
        cursor.execute(
            "SELECT 1 FROM Proveedor WHERE id_farmacia = %s", (id_proveedor,)
        )
        existe_proveedor = cursor.fetchone()
        if not existe_proveedor:
            conn.close()
            return jsonify({"error": "Proveedor no encontrado"}), 404

        # Elimina el proveedor de la base de datos
        query = "DELETE FROM Proveedor WHERE id_farmacia = %s"
        cursor.execute(query, (id_proveedor,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Proveedor eliminado con éxito"})

    except Exception as e:
        return jsonify({"error": str(e)})


# MEDICINA
@app.route("/medicina", methods=["GET"])
@jwt_required()
def obtener_medicina():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT id_medicina, nombre_producto, descripcion_producto, categoria_producto, precio_producto, n_disponibles, laboratorio_id, proveedor_id FROM Medicina"
        )
        medicinas = []

        for row in cursor.fetchall():
            medicina = {
                "id_medicina": row[0],
                "nombre_producto": row[1],
                "descripcion_producto": row[2],
                "categoria_producto": row[3],
                "precio_producto": row[4],
                "n_disponibles": row[5],
                "laboratorio_id": row[6],
                "proveedor_id": row[7],
            }
            medicinas.append(medicina)

        conn.close()
        return jsonify(medicinas)
    except Exception as e:
        return jsonify({"error": str(e)})


@app.route("/medicina", methods=["POST"])
@jwt_required()
def crear_medicina():
    try:
        data = request.json
        campos_obligatorios = [
            "nombre_producto",
            "descripcion_producto",
            "categoria_producto",
            "precio_producto",
            "n_disponibles",
        ]

        # Verifica que se proporcionen todos los campos obligatorios
        for campo in campos_obligatorios:
            if campo not in data:
                return jsonify({"error": f'El campo "{campo}" es obligatorio'}), 400

        # Verifica que n_disponibles sea un entero positivo
        n_disponibles = data["n_disponibles"]
        if not isinstance(n_disponibles, int) or n_disponibles < 0:
            return (
                jsonify(
                    {"error": 'El campo "n_disponibles" debe ser un entero positivo'}
                ),
                400,
            )

        conn = conectar()
        cursor = conn.cursor()
        query = "INSERT INTO Medicina (nombre_producto, descripcion_producto, categoria_producto, precio_producto, n_disponibles) VALUES (%s, %s, %s, %s, %s)"
        cursor.execute(
            query,
            (
                data["nombre_producto"],
                data["descripcion_producto"],
                data["categoria_producto"],
                data["precio_producto"],
                n_disponibles,
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Medicina creada con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para actualizar una medicina existente
@app.route("/medicina/<int:id_medicina>", methods=["PUT"])
@jwt_required()
def actualizar_medicina(id_medicina):
    try:
        data = request.json
        conn = conectar()
        cursor = conn.cursor()

        # Recupera la medicina existente de la base de datos
        query = "SELECT id_medicina, nombre_producto, descripcion_producto, categoria_producto, precio_producto, n_disponibles, laboratorio_id, proveedor_id FROM Medicina WHERE id_medicina = %s"
        cursor.execute(query, (id_medicina,))
        medicina_existente = cursor.fetchone()

        # Verifica que la medicina exista
        if medicina_existente is None:
            return jsonify({"error": "Medicina no encontrada"}), 404

        # Actualiza solo los campos proporcionados en la solicitud PUT
        if "nombre_producto" in data:
            medicina_existente["nombre_producto"] = data["nombre_producto"]
        if "descripcion_producto" in data:
            medicina_existente["descripcion_producto"] = data["descripcion_producto"]
        if "categoria_producto" in data:
            medicina_existente["categoria_producto"] = data["categoria_producto"]
        if "precio_producto" in data:
            medicina_existente["precio_producto"] = data["precio_producto"]
        if "n_disponibles" in data:
            medicina_existente["n_disponibles"] = data["n_disponibles"]

        # Actualiza los campos de la medicina en la base de datos
        query = "UPDATE Medicina SET nombre_producto = %s, descripcion_producto = %s, categoria_producto = %s, precio_producto = %s, n_disponibles = %s WHERE id_medicina = %s"
        cursor.execute(
            query,
            (
                medicina_existente["nombre_producto"],
                medicina_existente["descripcion_producto"],
                medicina_existente["categoria_producto"],
                medicina_existente["precio_producto"],
                medicina_existente["n_disponibles"],
                id_medicina,
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Medicina actualizada con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})


# Ruta para eliminar una medicina
@app.route("/medicina/<int:id_medicina>", methods=["DELETE"])
@jwt_required()
def eliminar_medicina(id_medicina):
    try:
        conn = conectar()
        cursor = conn.cursor()
        query = "DELETE FROM Medicina WHERE id_medicina = %s"
        cursor.execute(query, (id_medicina,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Medicina eliminada con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})

#Factura
# Ruta para obtener todas las facturas
@app.route("/factura", methods=["GET"])
@jwt_required()
def obtener_facturas():
    try:
        conn = conectar()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT id_factura, NFactura, fecha, medicina_id, cantidad_producto, descuento, totalAPagar, cliente_id, delivery_id, forma_pago, tfventa FROM Factura"
        )
        facturas = []
        for row in cursor.fetchall():
            facturas.append(
                {
                    "id_factura": row[0],
                    "NFactura": str(row[1]),
                    "fecha": str(row[2]),
                    "medicina_id": row[3],
                    "cantidad_producto": row[4],
                    "descuento": row[5],
                    "totalAPagar": float(row[6]),
                    "cliente_id": row[7],
                    "delivery_id": row[8],
                    "forma_pago": row[9],
                    "tfventa": row[10],
                }
            )
        conn.close()
        return jsonify(facturas)
    except Exception as e:
        return jsonify({"error": str(e)})

# Ruta para crear una nueva factura
@app.route("/factura", methods=["POST"])
@jwt_required()
def crear_factura():
    try:
        data = request.json
        if (
            "NFactura" not in data
            or "fecha" not in data
            or "medicina_id" not in data
            or "cantidad_producto" not in data
            or "descuento" not in data
            or "totalAPagar" not in data
            or "cliente_id" not in data
            or "delivery_id" not in data
            or "forma_pago" not in data
            or "tfventa" not in data
        ):
            return (
                jsonify(
                    {
                        "error": 'Todos los campos son obligatorios: "NFactura", "fecha", "medicina_id", "cantidad_producto", "descuento", "totalAPagar", "cliente_id", "delivery_id", "forma_pago", "tfventa"'
                    }
                ),
                400,
            )

        conn = conectar()
        cursor = conn.cursor()
        query = "INSERT INTO Factura (NFactura, fecha, medicina_id, cantidad_producto, descuento, totalAPagar, cliente_id, delivery_id, forma_pago, tfventa) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(
            query,
            (
                str(data["NFactura"]),
                data["fecha"],
                data["medicina_id"],
                data["cantidad_producto"],
                data["descuento"],
                data["totalAPagar"],
                data["cliente_id"],
                data["delivery_id"],
                data["forma_pago"],
                data["tfventa"],
            ),
        )
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Factura creada con éxito"})
    except Exception as e:
        return jsonify({"error": str(e)})

# Ruta para actualizar una factura existente
@app.route("/factura/<int:id_factura>", methods=["PUT"])
@jwt_required()
def actualizar_factura(id_factura):
    try:
        data = request.json
        conn = conectar()
        cursor = conn.cursor()

        cursor.execute(
            "SELECT 1 FROM Factura WHERE id_factura = %s", (id_factura,)
        )
        existe_factura = cursor.fetchone()
        if not existe_factura:
            conn.close()
            return jsonify({"error": "Factura no encontrada"}), 404

        campos_actualizados = {}
        if "NFactura" in data:
            campos_actualizados["NFactura"] = str(data["NFactura"])
        if "fecha" in data:
            campos_actualizados["fecha"] = data["fecha"]
        if "medicina_id" in data:
            campos_actualizados["medicina_id"] = data["medicina_id"]
        if "cantidad_producto" in data:
            campos_actualizados["cantidad_producto"] = data["cantidad_producto"]
        if "descuento" in data:
            campos_actualizados["descuento"] = data["descuento"]
        if "totalAPagar" in data:
            campos_actualizados["totalAPagar"] = data["totalAPagar"]
        if "cliente_id" in data:
            campos_actualizados["cliente_id"] = data["cliente_id"]
        if "delivery_id" in data:
            campos_actualizados["delivery_id"] = data["delivery_id"]
        if "forma_pago" in data:
            campos_actualizados["forma_pago"] = data["forma_pago"]
        if "tfventa" in data:
            campos_actualizados["tfventa"] = data["tfventa"]

        if campos_actualizados:
            query = "UPDATE Factura SET "
            query += ", ".join(
                [f"{campo} = %s" for campo in campos_actualizados.keys()]
            )
            query += " WHERE id_factura = %s"

            valores_actualizados = tuple(campos_actualizados.values()) + (id_factura,)
            cursor.execute(query, valores_actualizados)
            conn.commit()
            conn.close()
            return jsonify({"mensaje": "Factura actualizada con éxito"})
        else:
            conn.close()
            return jsonify({"mensaje": "No se proporcionaron datos para actualizar"})

    except Exception as e:
        return jsonify({"error": str(e)})

# Ruta para eliminar una factura
@app.route("/factura/<int:id_factura>", methods=["DELETE"])
@jwt_required()
def eliminar_factura(id_factura):
    try:
        conn = conectar()
        cursor = conn.cursor()

        cursor.execute(
            "SELECT 1 FROM Factura WHERE id_factura = %s", (id_factura,)
        )
        existe_factura = cursor.fetchone()
        if not existe_factura:
            conn.close()
            return jsonify({"error": "Factura no encontrada"}), 404

        query = "DELETE FROM Factura WHERE id_factura = %s"
        cursor.execute(query, (id_factura,))
        conn.commit()
        conn.close()
        return jsonify({"mensaje": "Factura eliminada con éxito"})

    except Exception as e:
        return jsonify({"error": str(e)})


if __name__ == "__main__":
    app.run(debug=True)
